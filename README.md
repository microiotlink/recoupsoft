# recoupsoft

#### 介绍
垃圾分类回收系统，包括后台系统，回收小程序，线下终端系统，包括硬件推荐使用，包括服务接线技术支持，系统源码级服务，源码级培训，保障上线

##### 演示地址：<br>
地址:https://shop.5197h.com/gxsys/login <br>
账号:demo<br>
密码:123456<br>

#### 沟通交流群

QQ群：591872856<br>

邮件交流：qixiu_demo@163.com<br>
微信交流: mengzhiguo831231<br>
QQ交流: 274797328<br>

### 软件介绍
 
#### 客户案例

<img src='https://shop.5197h.com/gxsys//profile/upload/mpconfig/code.jpg' width="35%" /><br>

<img src='https://shop.5197h.com/gxsys//profile/upload/mpconfig/code1.jpg' width="35%" /><br>
#### 用户技术交流群
<img src='https://shop.5197h.com/gxsys//profile/upload/mpconfig/z/z5.jpg' width="35%" /><br>

<img src='https://shop.5197h.com/gxsys//profile/upload/mpconfig/n/a1.jpg' width="45%"/>
 

<img src='https://shop.5197h.com/gxsys//profile/upload/mpconfig/n/a5.jpg' width="45%"/>
 
<img src='https://shop.5197h.com/gxsys//profile/upload/mpconfig/n/a7.jpg' width="45%"/>

<img src='https://shop.5197h.com/gxsys//profile/upload/mpconfig/d1.jpg' width="45%" />
<img src='https://shop.5197h.com/gxsys//profile/upload/mpconfig/d2.jpg' width="45%"/>
<img src='https://shop.5197h.com/gxsys//profile/upload/mpconfig/d3.jpg' width="45%"/>
<img src='https://shop.5197h.com/gxsys//profile/upload/mpconfig/d4.jpg' width="45%"/>
<img src='https://shop.5197h.com/gxsys//profile/upload/mpconfig/d6.jpg' width="45%"/>
<img src='https://shop.5197h.com/gxsys//profile/upload/mpconfig/d7.jpg' width="45%"/>
<img src='https://shop.5197h.com/gxsys//profile/upload/mpconfig/d8.jpg' width="45%"/>


<img src='https://shop.5197h.com/gxsys//profile/upload/mpconfig/z/z1.jpg' width="45%"/>
<img src='https://shop.5197h.com/gxsys//profile/upload/mpconfig/z/z2.jpg' width="45%"/>
<img src='https://shop.5197h.com/gxsys//profile/upload/mpconfig/z/z3.jpg' width="45%"/>
<img src='https://shop.5197h.com/gxsys//profile/upload/mpconfig/z/z4.jpg' width="45%"/>

#### 硬件设备说明

#####  主板介绍
 <img src='https://shop.5197h.com/gxsys//profile/upload/mpconfig/2.png' width="95%"/>




##### 主板主控卡

 <img src='https://shop.5197h.com/gxsys//profile/upload/mpconfig/3.png' width="95%"/>

 ##### 配套设备清单

 <img src='https://shop.5197h.com/gxsys//profile/upload/mpconfig/sb.png' width="95%"/>

#### 后台介绍

 
#### 后台操作说明

 <img src='https://shop.5197h.com/gxsys//profile/upload/mpconfig/c1.png' width="95%"/>
<img src='https://shop.5197h.com/gxsys//profile/upload/mpconfig/c2.png' width="95%"/>
<img src='https://shop.5197h.com/gxsys//profile/upload/mpconfig/c3.png' width="95%"/>
<img src='https://shop.5197h.com/gxsys//profile/upload/mpconfig/c4.png' width="95%"/>
<img src='https://shop.5197h.com/gxsys//profile/upload/mpconfig/d4.jpg' width="45%"/>
<img src='https://shop.5197h.com/gxsys//profile/upload/mpconfig/n/a4.jpg' width="45%"/>
#### 沟通交流群
QQ群：591872856(交流群)

邮件交流：qixiu_demo@163.com
 
